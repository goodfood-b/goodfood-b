//voir fichier webpack.config plus complet sur le net

const path = require("path");

// Require the new plugin
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: "production",
  // Change le point d'entrée en main.tsx
  entry: "./apps/goodfood-frontend/src/main.tsx",
  // Active le sourcemap pour le debugging
  devtool: "source-map",
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  },
  resolve: {
    // Ajoute '.ts' et'.tsx' aux extensions traitées
    extensions: [".ts", ".tsx", ".js", ".jsx", ".json"]
  },
  output: {
    path: path.join(__dirname, "/dist"),
    filename: "[name].bundle.js",
    clean: true
  },
  module: {
    rules: [
      // Tous les fichiers en `.ts` ou `.tsx` seront traités en utilisant `ts-loader`
      {
        test: /\.[jt]sx?$/,
        loader: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: "css-loader"
      },
      {
        test: /\.(gif|png|avif|jpe?g)$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "assets/images/"
        }
      }
    ]
  },
  plugins: [
  new HtmlWebpackPlugin({      // Instancie le plugin
    template: "./apps/goodfood-frontend/src/index.html"  // Spécifie notre template
  })
]
};
