import React from 'react'
import './Header.css';
import Logo from '../../../assets/images/GF.png';

export function Header({ titre, pictogramme }: { titre: string, pictogramme: string }) {
    return (
        <>
            <div className='header'>
                <div className='logo'>
                    <a href="/"><img src={Logo} alt="logo_goodfood" /></a>
                </div>
                <div className='logo_restaurant'>
                    <img src={pictogramme} />
                </div>
                <div className='titre'>
                    <h1>
                        {titre}
                    </h1>
                </div>
            </div>
        </>
    )
}

export default Header;
