import React, {Props} from 'react';
import {Button, Card} from "react-bootstrap";
import {formatCurrency} from "../../CartUtils/formatCurrency";
import {useShoppingCart} from "../../CartUtils/Context/shoppingCartContext";
import {ProductType} from "../../types/ProductType";

function ProductItem (props:{ product:any }) {
  let produit = props.product.product_id[0];

  const { addItemToCart } = useShoppingCart();
    return(
      <>
          <Card className="h-100">
          <Card.Img
          variant="top"
          src={produit.uri}
          height="200px"
          style={{objectFit: "cover"}}
          />
          <Card.Body className="d-flex flex-column">
          <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
          <span className="fs-2">{produit.name}</span>
          <span className="ms-2 text-muted">{produit.price}</span>
          </Card.Title>
          <div className="mt-auto">
          <Button className="w-100" onClick={() => addItemToCart(produit._id, produit.name, produit.price, produit.uri)}>
          + Add To Cart
          </Button>
          </div>
          </Card.Body>
          </Card>
      </>
    )
};

export default ProductItem;
