import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import ProductItem from './ProductItem'
import axios, {AxiosResponse} from 'axios'

const Products: React.FC = () => {
  const location = useLocation();
  const state = location.state;
  // @ts-ignore
  let id = state.id;
  const [products, setProducts] = useState<any[]>([]);


  useEffect(() => {
    //setProducts(getProducts(id));
    axios
      .get('https://good-food-back.herokuapp.com/goodfood/public/restaurant/' + id + '/products')
      .then((res) => setProducts(res.data))
  }, []);

  // @ts-ignore
  return (
    <>
      {products.map(product => (
          <ProductItem product={product} />
      ))}
    </>
  )
};

export default Products;
