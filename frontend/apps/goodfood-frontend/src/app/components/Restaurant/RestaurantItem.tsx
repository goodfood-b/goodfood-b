import Products from '../Product/Products';
import Route, { useNavigate} from 'react-router-dom';
import {Button, Card} from "react-bootstrap";
import React from "react";

export default function RestaurantItem(props: { name: string, url_logo: string, adresse: string, id: string}) {
  const navigate = useNavigate();

  const onClickReturnProduits = () => {
    navigate('/restaurant/produits', {state: {id: props.id, name: props.name}});
  }

  return(
    <Card className="h-100" onClick={onClickReturnProduits}>
      <Card.Img
        variant="top"
        src={props.url_logo}
        height="200px"
        style={{ objectFit: "cover" }}
      />
      <Card.Body className="d-flex flex-column">
        <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
          <span className="fs-2">{props.name}</span>
        </Card.Title>
        <Card.Text>
          {props.adresse}
        </Card.Text>
      </Card.Body>
    </Card>
  )
}

