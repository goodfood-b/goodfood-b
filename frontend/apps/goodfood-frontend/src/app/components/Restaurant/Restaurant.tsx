import React, {useState, useEffect} from 'react';
import axios from 'axios';
import RestaurantItem from './RestaurantItem';

export default function Restaurant() {
  const [restaurants, setRestaurants] = useState<any[]>([]);
  const baseUrl = 'https://good-food-back.herokuapp.com/goodfood/public/restaurants';
  useEffect(() => {
    axios.get(baseUrl).then((res) => {
      setRestaurants(res.data);
    })
  }, [])

  console.log(restaurants);

  return (
    <>
      <div className="item-restaurants">
        { restaurants.map(restaurant => {
          return <RestaurantItem id={restaurant.id} name={restaurant.nom} url_logo={restaurant.url_logo} adresse={restaurant.adresse}/>;
        })}
      </div>
    </>
  )
}
