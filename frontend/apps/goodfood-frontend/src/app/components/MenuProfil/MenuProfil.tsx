import React, { useState, useEffect } from 'react';
import {useNavigate} from "react-router";
import './MenuProfil.css';
import Badge from '@mui/material/Badge';
import NotificationsIcon from '@mui/icons-material/Notifications';
import PersonIcon from '@mui/icons-material/Person';
import IconButton from '@mui/material/IconButton';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import SearchIcon from '@mui/icons-material/Search';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import * as AuthService from "../../services/authService";
import {useShoppingCart} from "../../CartUtils/Context/shoppingCartContext";

export function MenuProfil() {

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [showMenuItems, setShowMenuItems] = useState<boolean>(false);
  const [showLoginItem, setShowLoginItem] = useState<boolean>(true);
  const [showLogoutItem, setShowLogoutItem] = useState<boolean>(false);
  const { openCart, cartQuantity } = useShoppingCart();
  const navigate = useNavigate();

  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      setShowMenuItems(true);
      setShowLoginItem(false);
      setShowLogoutItem(true);
    }
  }, []);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const connect = () => {
    navigate("/connexion");
    //window.location.reload();
  };

  const logOut = () => {
    AuthService.logout();
    setShowMenuItems(false);
    setShowLogoutItem(false);
    window.location.reload()
  };

  const goToStore = () => {
    navigate("/store");
  };

    return (
        <>
            <div className='MenuProfil'>
                <IconButton color="inherit">
                    <Badge badgeContent={8} color="secondary">
                        <NotificationsIcon fontSize="large" />
                    </Badge>
                </IconButton>

                <IconButton color="inherit">
                    <SearchIcon fontSize="large"/>
                </IconButton>

                <IconButton onClick={openCart} color="inherit">
                  <Badge color="secondary" badgeContent={cartQuantity}>
                    <ShoppingCartIcon fontSize="large"/>{" "}
                  </Badge>
                </IconButton>

                <IconButton onClick={handleClick} color="inherit">
                    <PersonIcon fontSize="large" />
                </IconButton>
                <Menu
                  PaperProps={{sx: {top: '140px'}}}
                  keepMounted
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  open={Boolean(anchorEl)}
                >
                  {showMenuItems && (
                  <MenuItem onClick={handleClose}>Mon Compte</MenuItem>
                  )}
                  {showMenuItems && (
                    <MenuItem onClick={handleClose}>Mon Profil</MenuItem>
                  )}
                  {showMenuItems && (
                  <MenuItem onClick={handleClose}>Mes Commandes</MenuItem>
                  )}
                  {showLoginItem && (
                    <MenuItem onClick={() => { handleClose(); connect();}}>Connexion</MenuItem>
                  )}
                  {showLogoutItem && (
                    <MenuItem onClick={() => { handleClose(); logOut();}}>Déconnexion</MenuItem>
                  )}
                </Menu>
            </div>
        </>
    )
}

export default MenuProfil;
