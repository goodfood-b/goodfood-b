import React, {useState, useEffect} from 'react'
import './MenuNavigation.css';

import PersonIcon from '@mui/icons-material/Person';
import IconButton from '@mui/material/IconButton';
import LocalPizzaIcon from '@mui/icons-material/LocalPizza';
import LunchDiningIcon from '@mui/icons-material/LunchDining';
import HomeIcon from '@mui/icons-material/Home';
import ListIcon from '@mui/icons-material/List';
import FoodBankIcon from '@mui/icons-material/FoodBank';
import LoyaltyIcon from '@mui/icons-material/Loyalty';
import DiscountIcon from '@mui/icons-material/Discount';
import HelpIcon from '@mui/icons-material/Help';
import ContactMailIcon from '@mui/icons-material/ContactMail';
import GavelIcon from '@mui/icons-material/Gavel';
import FaceIcon from '@mui/icons-material/Face';
import { getCurrentUser } from "../../services/authService";
import EventBus from "../../events/eventBus";

export function MenuNavigation() {

    const [message, setMessage] = useState<string>("");
    useEffect( () => {
        const currentUser = getCurrentUser();
        if(currentUser){
          setMessage(currentUser.firstName + " " + currentUser.lastName);
        }
    }, [message]);

    const handleLogOut = () => {
        setMessage("");
    };

    return (
        <>
            <nav className="main-menu">
                <ul>
                    <li>
                        <a href="/">
                            <i className="fa fa-2x">
                                <HomeIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Accueil
                            </span>
                        </a>

                    </li>
                    {/* <li className="has-subnav">
                    <a href="#">
                        <i className="fa fa-laptop fa-2x"></i>
                        <span className="nav-text">
                            Stars Components
                        </span>
                    </a>

                </li> */}
                    <li className="has-subnav">
                        <a href="/categories">
                            <i className="fa fa-2x">
                                <ListIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Toutes les categories
                            </span>
                        </a>

                    </li>

                    <li className="has-subnav">
                        <a href="/restaurants">
                            <i className="fa fa-2x">

                                <FoodBankIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Tous les restaurants
                            </span>
                        </a>

                    </li>
                    <li className="has-subnav">
                        <a href="#">
                            <i className="fa fa-2x">
                                <LocalPizzaIcon fontSize="medium" />
                            </i>

                            <span className="nav-text">

                                Pizzas
                            </span>
                        </a>

                    </li>

                    <li className="has-subnav">
                        <a href="#">
                            <i className="fa fa-2x">
                                <LunchDiningIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Burgers
                            </span>
                        </a>

                    </li>
                    <li className="has-subnav">
                        <a href="#">
                            <i className="fa fa-pizza fa-2x"></i>
                            <span className="nav-text">

                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="#">
                            <i className="fa fa-2x">
                                <LoyaltyIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Fidélité restaurant
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i className="fa fa-2x">
                                <DiscountIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Dernières offres
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i className="fa fa-2x">
                                <ContactMailIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Contact
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i className="fa fa-2x">
                                <GavelIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Conditions generales
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i className="fa fa-2x">
                                <HelpIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                                Aide
                            </span>
                        </a>
                    </li>
                </ul>
                <ul className="logout">
                    <li>
                        <a href="/connexion">
                            <i className="fa fa-2x">
                                <FaceIcon fontSize="medium" />
                            </i>
                            <span className="nav-text">
                              {message}
                            </span>
                        </a>
                    </li>
                </ul>
            </nav>
        </>
    )
}

export default MenuNavigation;
