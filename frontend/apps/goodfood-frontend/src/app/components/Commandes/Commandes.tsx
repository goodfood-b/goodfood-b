import React, { Component } from 'react';
import axios from 'axios';

export default class Commandes extends React.Component {
  override state = {
     commandes: []
  }

  override componentDidMount() {
    axios.get('http://127.0.0.1:8002/goodfood/commands')
      .then(res => {
        const commandes = res.data;
        this.setState({commandes})
      })
  }
  override render() {

    return <div>
      { this.state.commandes.map(commande => JSON.stringify(commande))}
    </div>;
  }
}
