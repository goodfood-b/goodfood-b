import React, {useState} from 'react';
import './commandDialog.css';
import {useNavigate} from "react-router";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {useShoppingCart} from "../../../CartUtils/Context/shoppingCartContext";
import UseDialog from "../../../CartUtils/useDialog";

type AlertProps = {
  isOpened: boolean;
  display: () => void;
  getTitle: () => string;
  getMessage: () => string;
  getLocation: () => string;
};

const AlertDialog: React.FC<AlertProps> = ({isOpened, display, getTitle, getMessage, getLocation}) => {

  const navigate = useNavigate();
  const { closeCart } = useShoppingCart();
  const { dialogNav } = UseDialog();

  const handleClose = () => {
    display();
    closeCart();
    navigate(getLocation());
    window.location.reload();
  };

  return (
    <>
      <div className="dialog-wrapper">
        <Dialog
          open={isOpened}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {getTitle()}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {getMessage()}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Fermer</Button>
          </DialogActions>
        </Dialog>
      </div>
    </>
  );
};

export default AlertDialog;
