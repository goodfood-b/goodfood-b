/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 This is a test
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

 import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
 import Header from '../../../components/Header/Header';
 import MenuNavigation from '../../../components/MenuNavigation/MenuNavigation';
 import MenuProfil from '../../../components/MenuProfil/MenuProfil';
import {ShoppingCartProvider} from "../../../CartUtils/Context/shoppingCartContext";
import React from "react";

//  import Pictogramme from '';

 export function Accueil() {
   return (

     <>
       <MenuNavigation />
       <Header titre='Accueil' pictogramme='' />
       <MenuProfil />
     </>
   );
 }

 export default Accueil;
