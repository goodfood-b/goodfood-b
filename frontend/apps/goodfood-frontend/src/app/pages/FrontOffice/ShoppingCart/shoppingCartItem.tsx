import './shoppingCart.css';
import { Button, Stack } from "react-bootstrap";
import DeleteIcon from '@mui/icons-material/Delete';
import { useShoppingCart } from "../../../CartUtils/Context/shoppingCartContext";
import { formatCurrency } from "../../../CartUtils/formatCurrency";
import React from "react";
import {IconButton} from "@mui/material";

type CartItemProps = {
  id: string;
  name?: string;
  price?: number
  quantity: number;
  uri?: string
}

const CartItem: React.FC<CartItemProps> = ({ id, name, price, quantity, uri}) => {

  const {
    increaseCartQuantity,
    decreaseCartQuantity,
    removeFromCart,
  } = useShoppingCart();

  return (
    <Stack direction="horizontal" gap={2} className="d-flex align-items-center">
      <div className="cart-item">
        <img src={uri} />
        <div className="cart-item-infos">
          <div className="cart-item-name">
              {name}{" "}
          </div>
          <div className="cart-item-price">
              Prix: {formatCurrency((price == undefined)?0:price)}
          </div>
        </div>
        <div className="handle-qty">
          <Button onClick={() => decreaseCartQuantity(id)}>-</Button>
          <div>
            <span className="cart-item-qty">{quantity}</span>
          </div>
          <Button onClick={() => increaseCartQuantity(id)}>+</Button>
        </div>
        <div className="cart-line-total">
          {formatCurrency((price == undefined)?0:price * quantity)}
        </div>
        <IconButton onClick={() => removeFromCart(id)}>
          <DeleteIcon fontSize="small" />
        </IconButton>
      </div>
    </Stack>
  )
};
export default CartItem;
