import './shoppingCart.css';
import React, {useState} from "react";
import CartItem from './shoppingCartItem';
import Drawer from "@material-ui/core/Drawer/Drawer";
import {useShoppingCart} from "../../../CartUtils/Context/shoppingCartContext";
import {formatCurrency} from "../../../CartUtils/formatCurrency";
import {getCurrentUser} from "../../../services/authService";
import {clearShoppingCart, getShoppingCart, sendCommand} from "../../../services/commandService";
import {CommandLine} from "../../../types/commandLineType";
import AlertDialog from "../Command/commandDialog";
import UseDialog from '../../../CartUtils/useDialog';

type Props = {
  isOpen: boolean;
};

const Cart: React.FC<Props> = ({isOpen}) => {
  const { closeCart, cartItems} = useShoppingCart();
  const {isOpened, display, setDialogContent} = UseDialog();
  const [message, setMessage] = useState<string>("");
  const [dialogTitle, setDialogTitle] = useState<string>("");
  const [redirect, setRedirect] = useState<string>("");

  const handleSendCommand = () => {
    const commandLines: CommandLine[] = getShoppingCart();
    const user = getCurrentUser();
    if(user){
      const clientLastName = user.lastName;
      const clientFirstName = user.firstName;
      const clientRef = user.id;
      const clientEmail = user.email;
      clearShoppingCart();
      sendCommand(clientLastName, clientFirstName, clientEmail, clientRef, commandLines).then(
        (response) => {
          setMessage(response.data.message);
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          setMessage(resMessage);
        }
      );

      setDialogTitle("Commande expédiée");
      setRedirect("/accueil");
      display();
    } else{
      setDialogTitle("Authentification requise");
      setMessage("Veuillez vous connecter afin de pouvoir valider votre commande.Si vous ne possédez pas de compte, merci de bien vouloir vous inscrire.");
      setRedirect("/connexion");
      display();
    }

  };

  const getDialogTitle= () => {
    return dialogTitle;
  };

  const getDialogMessage = () => {
    return message;
  };

  const getLocation = () => {
    return redirect;
  };

  return (
    <>
      <Drawer anchor='right' open={isOpen} onClose={closeCart} >
          <div className='cartWrapper'>

            <h2>Panier</h2>
            {cartItems.map(item => (
              <CartItem key={item.id} {...item} />
            ))}
            <div className="total-cart">
              <h3 className="total-cart-label">Total Panier: {" "}</h3>
                {formatCurrency (cartItems.reduce((total, cartItem) => {
                  return total + (cartItem?.price || 0) * cartItem.quantity
                }, 0))}
            </div>
            <div className="send-order">
              <button onClick={handleSendCommand}>Commander</button>
            </div>
          </div>
      </Drawer>
      {isOpened && (
        <AlertDialog isOpened={isOpened} display={display} getTitle={getDialogTitle} getMessage={getDialogMessage} getLocation={getLocation}/>
      )}

    </>
  );
};

export default Cart;
