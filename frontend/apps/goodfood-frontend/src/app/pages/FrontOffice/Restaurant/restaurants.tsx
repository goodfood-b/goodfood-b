import Header from '../../../components/Header/Header';
import MenuNavigation from '../../../components/MenuNavigation/MenuNavigation';
import MenuProfil from '../../../components/MenuProfil/MenuProfil';
import Restaurant from '../../../components/Restaurant/Restaurant';
import './Restaurant.css';
import React from "react";

export function Restaurants() {
  return (
    <>
      <MenuNavigation />
      <Header titre='Restaurants' pictogramme='' />
      <MenuProfil />
      <div className="restaurants-container">
        <Restaurant />
      </div>
    </>
  )
}

export default Restaurants;
