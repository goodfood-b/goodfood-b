import { useParams } from "react-router-dom";
import React from "react";


export function Categories() {
  const { typeCategories } = useParams()
  return (

    <>
      <div className="wrapper">
        <div className="container">
          <div id="welcome">
          </div>
          <div>
          </div>

          <div id="hero" className="rounded">
            <div className="text-container">

              <h2>La categorie est : {typeCategories}</h2>

            </div>

          </div>
        </div>
      </div>
    </>
  );
}

export default Categories;
