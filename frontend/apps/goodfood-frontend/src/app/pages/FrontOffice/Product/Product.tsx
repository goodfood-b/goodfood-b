import MenuNavigation from "../../../components/MenuNavigation/MenuNavigation";
import Header from "../../../components/Header/Header";
import MenuProfil from "../../../components/MenuProfil/MenuProfil";
import Products from "../../../components/Product/Products";
import React from "react";
import './Product.css';
import {useLocation} from "react-router-dom";

export default function Product() {
  return(
    <>
      <MenuNavigation />
      <Header titre="" pictogramme='' />
      <MenuProfil />
      <div className="products-container">
        <Products />
      </div>
    </>
  )
}
