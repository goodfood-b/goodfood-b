import React, { useState } from "react";
import {useNavigate} from "react-router";
import './connexion.css';
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import IUser from "../../types/userType";
import {login, register} from "../../services/authService";
import MenuNavigation from "../../components/MenuNavigation/MenuNavigation";
import Header from "../../components/Header/Header";
import MenuProfil from "../../components/MenuProfil/MenuProfil";

const Connexion: React.FC = () => {
  const [successful, setSuccessful] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const navigate = useNavigate();

  const initialLoginValues: {
    email: string;
    password: string;
  } = {
    email: "",
    password: "",
  };

  const registerValuesInitial: IUser = {
    firstname: "",
    lastname: "",
    birthdate: "",
    email: "",
    password: "",
  };

  const validationLoginSchema = Yup.object().shape({
    email: Yup.string().required("Ce champ est requis!"),
    password: Yup.string().required("Ce champ est requis!"),
  });

  const handleLogin = (formValue: { email: string; password: string }) => {
    const { email, password } = formValue;
    setMessage("");
    setLoading(true);
    login(email, password).then(
      () => {
        navigate("/accueil");
        window.location.reload();
      },
      (error) => {
        const resLoginMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        setLoading(false);
        setMessage(resLoginMessage);
      }
    );
  };

  const validationRegisterSchema = Yup.object().shape({
    firstname: Yup.string()
      .test(
        "len",
        "Le prénom doit contenir entre 3 et 20 caractères",
        (val: any) =>
          val &&
          val.toString().length >= 3 &&
          val.toString().length <= 20
      )
      .required("Ce champ est requis!"),
    lastname: Yup.string()
      .test(
        "len",
        "Le nom doit contenir entre 3 et 20 caractères",
        (val: any) =>
          val &&
          val.toString().length >= 3 &&
          val.toString().length <= 20
      )
      .required("Ce champ est requis!"),
    birthdate: Yup.string().required("Veuillez indiquer une date de naissance"),
    email: Yup.string()
      .email("Ce n'est pas un email valide")
      .required("Ce champ est requis!"),
    password: Yup.string()
      .test(
        "len",
        "Le mot de passe doit contenir entre 6 and 40 caractères.",
        (val: any) =>
          val &&
          val.toString().length >= 6 &&
          val.toString().length <= 40
      )
      .required("Ce champ est requis!")
  });

  const handleRegister = (formValue: IUser) => {
    const { firstname, lastname, birthdate, email, password } = formValue;

    register(firstname, lastname, birthdate, email, password).then(
      (response) => {
        setMessage(response.data.message);
        setSuccessful(true);
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        setMessage(resMessage);
        setSuccessful(false);
      }
    );
  };

  return (
    <>
      <MenuNavigation />
      <Header titre='Connexion' pictogramme='' />
      <MenuProfil />

      <div className="col-login">
        <p>Connectez-vous</p>
        <div className="login-container">
          <Formik
            initialValues={initialLoginValues}
            validationSchema={validationLoginSchema}
            onSubmit={handleLogin}
          >
            <Form>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <Field type="text" name="email" id="email"/>
                <ErrorMessage
                  name="email"
                  component="div"
                  className="alert-danger"
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Mot de passe</label>
                <Field type="password" name="password" id="password" />
                <ErrorMessage
                  name="password"
                  component="div"
                  className="alert-danger"
                />
              </div>
              <div id="form-group-btn">
                <button type="submit" className="btn-submit" disabled={loading}>
                  {loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Connexion</span>
                </button>
              </div>
              {message && (
                <div className="form-group">
                  <div className={successful ? "alert-success" : "alert-danger"} role="alert">
                    {message}
                  </div>
                </div>
              )}
            </Form>
          </Formik>
        </div>
      </div>

      <div className="col-register">
        <p>Créez votre compte</p>
          <div className="register-container">
            <Formik
              initialValues={registerValuesInitial}
              validationSchema={validationRegisterSchema}
              onSubmit={handleRegister}
            >
              <Form>
                {!successful && (
                  <div>
                    <div className="form-group">
                      <label htmlFor="firstname"> Prénom </label>
                      <Field name="firstname" type="text" className="form-control" />
                      <ErrorMessage
                        name="firstname"
                        component="div"
                        className="alert alert-danger"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="lastname"> Nom </label>
                      <Field name="lastname" type="text" className="form-control" />
                      <ErrorMessage
                        name="lastname"
                        component="div"
                        className="alert alert-danger"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="birthdate"> Date de naissance </label>
                      <Field name="birthdate" type="text" className="form-control" />
                      <ErrorMessage
                        name="birthdate"
                        component="div"
                        className="alert alert-danger"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="email"> Email </label>
                      <Field name="email" type="email" className="form-control" />
                      <ErrorMessage
                        name="email"
                        component="div"
                        className="alert alert-danger"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="password"> Mot de passe </label>
                      <Field
                        name="password"
                        type="password"
                        className="form-control"
                      />
                      <ErrorMessage
                        name="password"
                        component="div"
                        className="alert alert-danger"
                      />
                    </div>

                    <div id="form-group-btn">
                      <button type="submit" aria-label="button-connexion" className="btn-submit">Enregistrement</button>
                    </div>
                  </div>
                )}

                {message && (
                  <div className="form-group">
                    <div className={successful ? "alert alert-success" : "alert alert-danger"} role="alert">
                      {message}
                    </div>
                  </div>
                )}
              </Form>
            </Formik>
          </div>
      </div>
    </>
  );
};

export default Connexion;
