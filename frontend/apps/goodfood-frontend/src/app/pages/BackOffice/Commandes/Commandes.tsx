import React from 'react'
import './Commandes.css';
import Header from '../../../components/Header/Header';
import MenuNavigation from '../../../components/MenuNavigation/MenuNavigation';
import MenuProfil from '../../../components/MenuProfil/MenuProfil';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';


function createData(
  id: number,
  etatPreparation: string,
  date: string,
  prenom: string,
  nom: string,
  prix: number,
  //produits: array(date, costrumerId, amount),
) {
  return {
    id,
    etatPreparation,
    date,
    prenom,
    nom,
    prix,
    produits: [
      {
        Nombre: 1,
        Produit: 'Burger Italien',
        Prix: 8,
      },
      {
        Nombre: 1,
        Produit: 'Coca-cola',
        Prix: 2,
      },
    ],
  };
}

function Row(props: { row: ReturnType<typeof createData> }) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {row.id}
        </TableCell>
        <TableCell align="center">{row.etatPreparation}</TableCell>
        <TableCell align="center">{row.date}</TableCell>
        <TableCell align="center">{row.prenom}</TableCell>
        <TableCell align="center">{row.nom}</TableCell>
        <TableCell align="center">{row.prix}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                Produits
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell align="center">Nombre</TableCell>
                    <TableCell align="center">Produit</TableCell>
                    <TableCell align="center">Prix</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.produits.map((produitsRow) => (
                    <TableRow key={produitsRow.Nombre}>
                      <TableCell align="center" component="th" scope="row">
                        {produitsRow.Nombre}
                      </TableCell>
                      <TableCell align="center">{produitsRow.Produit}</TableCell>
                      <TableCell align="center">
                        {produitsRow.Prix}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

const rows = [
  createData(12,'En cours', '24/07/2022 13h12',  'Lucas', 'Dupont', 9.90),
  createData(13, 'En cours', '24/07/2022 13h01', 'Jerome', 'Louis', 14.30),
  createData(14,'En cours', '24/07/2022 12h07', 'Juliette', 'Vinnel', 16.00),
  createData(15,'En cours', '24/07/2022 12h04', 'Damien', 'Pecou', 42.30),
  createData(15,'Termine', '24/07/2022 11h58', 'Michel', 'Toto', 38.90),
];


export function Commandes() {

    return (
        <>


      <MenuNavigation />
      <Header titre='Commandes' pictogramme='' />
      <MenuProfil />

            <div className='commandes'>

            <TableContainer component={Paper}>
                  <Table aria-label="collapsible table">
                    <TableHead>
                      <TableRow>
                        <TableCell />
                        <TableCell align="center">Reference</TableCell>
                        <TableCell align="center">Etat Preparation</TableCell>
                        <TableCell align="center">Date</TableCell>
                        <TableCell align="center">Prenom</TableCell>
                        <TableCell align="center">Nom</TableCell>
                        <TableCell align="center">Prix&nbsp;(€)</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {rows.map((row) => (
                        <Row key={row.id} row={row} />
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>




            </div>
        </>
    )
}

export default Commandes;
