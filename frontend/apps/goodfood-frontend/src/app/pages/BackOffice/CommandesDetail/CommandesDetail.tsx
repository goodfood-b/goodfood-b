import React, { useState } from "react";
import './CommandesDetails.css';
import Header from '../../../components/Header/Header';
import MenuNavigation from '../../../components/MenuNavigation/MenuNavigation';
import Commande from '../../../components/Commandes/Commandes';
import MenuProfil from '../../../components/MenuProfil/MenuProfil';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import Stack from '@mui/material/Stack';
import HourglassBottomIcon from '@mui/icons-material/HourglassBottom';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
//import useStateWithCallback from 'use-state-with-callback';
import {
  DataGridPro,
  GridColumns,
  useGridApiContext,
  GRID_DETAIL_PANEL_TOGGLE_FIELD,
} from '@mui/x-data-grid-pro';
import {
  randomCreatedDate,
  randomPrice,
  randomCountry,
  randomCity,
  randomEmail,
  randomInt,
  randomCommodity,
} from '@mui/x-data-grid-generator';
import axios from 'axios';




  //console.log(this.state.commandes);

//fetch('http://localhost:8080/goodfood/public/commands')
//.then(response=> response.json())


function DetailPanelContent({ row: rowProp }: { row: Customer }) {
  const apiRef = useGridApiContext();
  const [width, setWidth] = React.useState(() => {
    const dimensions = apiRef.current.getRootDimensions();
    return dimensions!.viewportInnerSize.width;
  });


  const handleViewportInnerSizeChange = React.useCallback(() => {
    const dimensions = apiRef.current.getRootDimensions();
    setWidth(dimensions!.viewportInnerSize.width);
  }, [apiRef]);

  React.useEffect(() => {
    return apiRef.current.subscribeEvent(
      'viewportInnerSizeChange',
      handleViewportInnerSizeChange,
    );
  }, [apiRef, handleViewportInnerSizeChange]);
  return (
      <Stack
        sx={{
          py: 2,
          height: '100%',
          boxSizing: 'border-box',
          position: 'sticky',
          left: 0,
          width,
        }}
        direction="column"
      >
        <Paper sx={{ flex: 1, mx: 'auto', width: '80%', p: 1 }}>
          <Stack direction="column" spacing={1} sx={{ height: 1 }}>
            <Typography variant="h6">{`Produits de la commande`}</Typography>
            <DataGridPro
              density="compact"
              columns={[
                { field: 'name', headerName: 'Produit', flex: 1, type: 'string' },
                {
                  field: 'quantity',
                  headerName: 'Quantité',
                  align: 'center',
                  type: 'number',
                },
                { field: 'price', headerName: 'Prix unitaire', type: 'number' }

              ]}
              rows={rowProp.commandLines}
              sx={{ flex: 1 }}
              hideFooter
            />
          </Stack>
        </Paper>
      </Stack>
    );
  }

  const columns: GridColumns = [
    { field: '_id', headerName: 'Reference', width: 220 },
    { field: 'preparationStateRef', headerName: 'Etat Preparation', width: 150},
    { field: 'cmdDate', type: 'date', headerName: 'Création commande', width: 250 },
    { field: 'clientFirstName', headerName: 'Prenom Client', width: 150 },
    { field: 'clientLastName', headerName: 'Nom Client', width: 150 },
    { field: 'clientEmail', headerName: 'Email Client', width: 250},
    {
      field: 'totalCommande',
      headerName: 'Total', width: 100, type: 'number'
    },
  ];

  const rows = [
                     {
                         "_id": "628b989d8affc0d3bc9d8962",
                         "cmdDate": "2014-12-11T14:12:00.000+00:00",
                         "clientFirstName": "Grand",
                         "clientLastName": "Lucas",
                         "clientEmail": "lucas.grand@yahoo.fr",
                         "totalCommande": "9.9",
                         etatPreparation: 'Envoye',
                         "products":  "test",
                         "commandLines": [
                             {
                                 "name": "Coca",
                                 "quantity": "1.0",
                                 "price": "2.0",
                                 "total": "9.9",
                                 "id": "248.0"
                             },
                             {
                                 "name": "Hamburger",
                                 "quantity": "1.0",
                                 "price": "7.9",
                                 "total": "9.9",
                                 "id": "145.0"
                             }
                         ]
                     }


  ];

   //function generateProducts() {
    //  const quantité = randomInt(1, 5);
     // return [...Array(rows2[0].lignesCommande)].map((_, index) => ({
     //   id: index,
     //   produit: randomCommodity(),
      //  quantité: randomInt(1, 5),
    //    unitPrice: randomPrice(1, 30),
    //    reference: randomCity()

    //  }));
 //   }

//console.log(generateProducts());
  const rows1 = [
    {
      id: "1",
      etatPreparation: 'En Attente',
      creation: randomCreatedDate(),
      nomClient: 'Matheus',
      prenomClient: 'Pirlo',
      emailClient: randomEmail(),
      country: randomCountry(),
      total : randomPrice(1, 30),
      lignesCommande: [{"id": 1, "produit": 'test',
                              "quantité": 5,
                              "unitPrice": 2, "reference": "test"}, {"id": 2, "produit": 'test2',
                                                                                                  "quantité": 4,
                                                                                                  "unitPrice": 2, "reference": "test2"}]
    },
    {
      id: 2,
      etatPreparation: 'En Attente',
      creation: randomCreatedDate(),
      nomClient: 'Olivier',
      prenomClient: 'Pirlo',
      emailClient: randomEmail(),
      country: randomCountry(),
      total : randomPrice(1, 30),
       lignesCommande: [{"id": 1, "produit": 'test',
                                    "quantité": 5,
                                    "unitPrice": 2, "reference": "test"}, {"id": 2, "produit": 'test2',
                                                                                                        "quantité": 4,
                                                                                                        "unitPrice": 2, "reference": "test2"}]
    },
    {
      id: 3,
      etatPreparation: 'En Attente',
      creation: randomCreatedDate(),
      nomClient: 'Flavien',
      prenomClient: 'Pirlo',
      emailClient: randomEmail(),
      country: randomCountry(),
      total : randomPrice(1, 30),
       lignesCommande: [{"id": 1, "produit": 'test',
                                    "quantité": 5,
                                    "unitPrice": 2, "reference": "test"}, {"id": 2, "produit": 'test2',
                                                                                                        "quantité": 4,
                                                                                                        "unitPrice": 2, "reference": "test2"}]
    },
    {
      id: 4,
      etatPreparation: 'En Cours',
      creation: randomCreatedDate(),
      nomClient: 'Danail',
      prenomClient: 'Pirlo',
      emailClient: randomEmail(),
      country: randomCountry(),
      total : randomPrice(1, 30),
       lignesCommande: [{"id": 1, "produit": 'test',
                                    "quantité": 5,
                                    "unitPrice": 2, "reference": "test"}, {"id": 2, "produit": 'test2',
                                                                                                        "quantité": 4,
                                                                                                        "unitPrice": 2, "reference": "test2"}]
    },
    {
      id: 5,
      etatPreparation: 'Livré',
      creation: randomCreatedDate(),
      nomClient: 'Alexandre',
      prenomClient: 'Pirlo',
      emailClient: randomEmail(),
      country: randomCountry(),
      total : randomPrice(1, 30),
       lignesCommande: [{"id": 1, "produit": 'test',
                                    "quantité": 5,
                                    "unitPrice": 2, "reference": "test"}, {"id": 2, "produit": 'test2',
                                                                                                        "quantité": 4,
                                                                                                        "unitPrice": 2, "reference": "test2"}]
    },
  ];
  type Customer = typeof rows[number];

export function CommandesDetails() {


const [message, setMessage] = useState([]);
//const kiki = [1,2];

axios.get('https://good-food-back.herokuapp.com/goodfood/public/commands')
        .then(res => {
           const rowst = res.data;
           setMessage(rowst);
        })


console.log(message);
const getDetailPanelContent = React.useCallback(
    ({ row }) => <DetailPanelContent row={row} />,
    [],
  );

  const getDetailPanelHeight = React.useCallback(() => 400, []);

    return (
        <>


<Commande/>
      <MenuNavigation />
      <Header titre='Commandes Detail' pictogramme='' />
      <MenuProfil />

            <div className='commandes'>

<Box sx={{ width: '100%', height: 500 }}>
      <DataGridPro
        columns={columns}
        //rows={[{"id":"6320d44b5bedcb797b59968d","cmdDate":"Tue Sep 13 21:04:43 CEST 2022","clientLastName":"Jacques","clientFirstName":"Martin", "clientRef":null,"clientEmail":null,"totalCommande":13.4,"commandLines":[{"name":"Menu Chicken Louisiane Steakhouse","id":"22","price":8.9,"quantity":1,"totalLineCommand":8.9},{"name":"Big Fish","id":"1","price":4.5,"quantity":1,"totalLineCommand":4.5}],"preparationStateRef":{"id":"6306797e5bc3ce4d841c2e84","name":"NOUVEAU"}},{"id":"6320d4cf5bedcb797b59968e","cmdDate":"Tue Sep 13 21:06:55 CEST 2022","clientLastName":"Jacques","clientFirstName":"Martin","clientRef":null,"clientEmail":null,"totalCommande":26.8,"commandLines":[{"name":"Big Fish","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":4.5,"quantity":2,"totalLineCommand":9},{"name":"Menu Chicken Louisiane Steakhouse","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":8.9,"quantity":2,"totalLineCommand":17.8}],"preparationStateRef":{"id":"6306797e5bc3ce4d841c2e84","name":"NOUVEAU"}},{"id":"6322114761ada801e470e80c","cmdDate":"Wed Sep 14 19:37:11 CEST 2022","clientLastName":"Jacques","clientFirstName":"Martin","clientRef":null,"clientEmail":null,"totalCommande":35.699997,"commandLines":[{"name":"Menu Chicken Louisiane Steakhouse","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":8.9,"quantity":3,"totalLineCommand":26.699999},{"name":"Big Fish","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":4.5,"quantity":2,"totalLineCommand":9}],"preparationStateRef":{"id":"6306797e5bc3ce4d841c2e84","name":"NOUVEAU"}},{"id":"6322493661ada801e470e816","cmdDate":"Wed Sep 14 23:35:50 CEST 2022","clientLastName":"Jacques","clientFirstName":"Martin","clientRef":null,"clientEmail":null,"totalCommande":72,"commandLines":[{"name":"Big Fish","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":4.5,"quantity":16,"totalLineCommand":72}],"preparationStateRef":{"id":"6306797e5bc3ce4d841c2e84","name":"NOUVEAU"}},{"id":"632249f461ada801e470e817","cmdDate":"Wed Sep 14 23:39:00 CEST 2022","clientLastName":"Jacques","clientFirstName":"Martin","clientRef":null,"clientEmail":null,"totalCommande":26.8,"commandLines":[{"name":"Menu Chicken Louisiane Steakhouse","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":8.9,"quantity":2,"totalLineCommand":17.8},{"name":"Big Fish","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":4.5,"quantity":2,"totalLineCommand":9}],"preparationStateRef":{"id":"6306797e5bc3ce4d841c2e84","name":"NOUVEAU"}},{"id":"63224d5261ada801e470e818","cmdDate":"Wed Sep 14 23:53:22 CEST 2022","clientLastName":"Jacques","clientFirstName":"Martin","clientRef":null,"clientEmail":null,"totalCommande":31.199999,"commandLines":[{"name":"Big Fish","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":4.5,"quantity":1,"totalLineCommand":4.5},{"name":"Menu Chicken Louisiane Steakhouse","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":8.9,"quantity":3,"totalLineCommand":26.699999}],"preparationStateRef":{"id":"6306797e5bc3ce4d841c2e84","name":"NOUVEAU"}},{"id":"6322f5ce2813ed3a6ef2de41","cmdDate":"Thu Sep 15 11:52:14 CEST 2022","clientLastName":"Jacques","clientFirstName":"Martin","clientRef":null,"clientEmail":null,"totalCommande":133.9,"commandLines":[{"name":"Big Fish","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":4.5,"quantity":8,"totalLineCommand":36},{"name":"Menu Chicken Louisiane Steakhouse","id":{"timestamp":1654198924,"date":"2022-06-02T19:42:04.000+00:00"},"price":8.9,"quantity":11,"totalLineCommand":97.899994}],"preparationStateRef":{"id":"6306797e5bc3ce4d841c2e84","name":"NOUVEAU"}}]}
        rows={message}
        rowThreshold={0}
        getRowId={(rows) => rows._id }
        pinnedColumns={{ left: [GRID_DETAIL_PANEL_TOGGLE_FIELD] }}
        getDetailPanelHeight={getDetailPanelHeight}
        getDetailPanelContent={getDetailPanelContent}
        checkboxSelection
      />
    </Box>
         <Tooltip title="En cours">
                  <IconButton>
                     <HourglassBottomIcon />
                  </IconButton>
        </Tooltip>
    <Tooltip title="Validé">
              <IconButton>
                <CheckCircleOutlineIcon />
              </IconButton>
            </Tooltip>




            </div>
        </>
    )
}

export default CommandesDetails;
