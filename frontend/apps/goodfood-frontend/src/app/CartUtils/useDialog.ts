import {useState} from "react";


const UseDialog = () => {

    const [isOpened, setIsOpened] = useState(false);
    const [dialogNav, setDialogNav] = useState<string>("");

    const display = () => {
      setIsOpened(!isOpened);
    };

    const setDialogContent = (location: string) => {
      setDialogNav(location);
  };

    return {
      isOpened,
      dialogNav,
      setDialogContent,
      display
    };

};

export default UseDialog;
