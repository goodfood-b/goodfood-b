import React, { createContext, ReactNode, useContext, useState } from "react"
import Cart from "../../pages/FrontOffice/ShoppingCart/shoppingCart"
import UseLocalStorage from "../useLocalStorage"

type ShoppingCartProviderProps = {
  children: ReactNode
}

type CartItem = {
  id: string
  quantity: number
  name?: string
  price?: number
}

type ShoppingCartContext = {
  openCart: () => void
  closeCart: () => void
  getItemQuantity: (id: string) => number
  addItemToCart: (id: string, name: string, price: number, uri: string) => void
  increaseCartQuantity: (id: string) => void
  decreaseCartQuantity: (id: string) => void
  removeFromCart: (id: string) => void
  removeAllFromCart: (ids: string[]) => void
  cartItems: CartItem[]
  cartQuantity: number
}

const ShoppingCartContext = createContext({} as ShoppingCartContext);

export const useShoppingCart = () => {
  return useContext(ShoppingCartContext)
};

export const ShoppingCartProvider: React.FC<ShoppingCartProviderProps> = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [cartItems, setCartItems] = UseLocalStorage<CartItem[]>(
    "shopping-cart",
    []
  );
  const cartQuantity = cartItems.reduce(
    (quantity, item) => item.quantity + quantity,
    0
  );

  const openCart = () => setIsOpen(true);
  const closeCart = () => setIsOpen(false);

  const  getItemQuantity = (id: string) => {
    return cartItems.find(item => item.id === id)?.quantity || 0
  };

  const increaseCartQuantity = (id: string) => {
    setCartItems(currItems =>{
      if (currItems.find(item => item.id === id) == null) {
        return [...currItems, { id, quantity: 1}]
      } else {
        return currItems.map(item => {
          if (item.id === id) {
            return { ...item, quantity: item.quantity + 1 }
          } else {
            return item
          }
        })
      }
  })
  };

  const decreaseCartQuantity = (id: string) => {
    setCartItems(currItems => {
      if (currItems.find(item => item.id === id)?.quantity === 1) {
        return currItems.filter(item => item.id !== id)
      } else {
        return currItems.map(item => {
          if (item.id === id) {
            return {...item, quantity: item.quantity - 1}
          } else {
            return item
          }
        })
      }
    });
  };
 const addItemToCart = (id: string, name: string, price: number, uri: string) => {
    setCartItems(currItems =>{
      if (currItems.find(item => item.id === id) == null) {
        return [...currItems, { id, quantity: 1, name: name, price: price, uri}]
      } else {
        return currItems.map(item => {
          if (item.id === id) {
            return { ...item, quantity: item.quantity +1}
          } else {
            return item
          }
        })
      }
    })
  };

  const removeFromCart = (id: string) => {
    setCartItems(currItems => {
      return currItems.filter(item => item.id !== id)
    })
  };

  const removeAllFromCart = (ids: string[]) => {
    for(let i = 0; i < ids.length; i++){
      setCartItems(currItems => {
        return currItems.filter(item => item.id !== ids[i]);
      })
    }
  };

    return (
      <ShoppingCartContext.Provider
        value={{
          getItemQuantity,
          addItemToCart,
          increaseCartQuantity,
          decreaseCartQuantity,
          removeFromCart,
          removeAllFromCart,
          openCart,
          closeCart,
          cartItems,
          cartQuantity
        }}
      >
        {children}
        <Cart isOpen={isOpen} />
      </ShoppingCartContext.Provider>
    )
};
