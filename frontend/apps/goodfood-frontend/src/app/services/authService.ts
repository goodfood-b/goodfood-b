import axios from "axios";

const API_URL = "https://good-food-back.herokuapp.com/goodfood/auth/";
const role = ["ROLE_CLIENT"];

export const register = (firstName: string, lastName: string, birthDate: string, email: string, password: string) => {
  return axios.post(API_URL + "signup", {
    firstName,
    lastName,
    birthDate,
    email,
    password,
    role
  });
};

export const login = (email: string, password: string) => {
  return axios
    .post(API_URL + "signin", {
      email,
      password,
    })
    .then((response) => {
      if (response.data.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }

      return response.data;
    });
};

export const logout = () => {
  localStorage.removeItem("user");
};

export const getCurrentUser = () => {
  const userStr = localStorage.getItem("user");
  if (userStr) return JSON.parse(userStr);

  return null;
};
