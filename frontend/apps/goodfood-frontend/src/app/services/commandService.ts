import axios from "axios";
import {CommandLine} from "../types/commandLineType";

const API_URL = "https://good-food-back.herokuapp.com/goodfood/public/";

export const sendCommand = (clientFirstName: string, clientLastName: string, clientEmail: string, clientRef: string,
                            commandLines: CommandLine[]) => {

  return axios.post(API_URL + "savecommand", {
    clientLastName,
    clientFirstName,
    clientEmail,
    clientRef,
    commandLines
  });
};

export const getShoppingCart = () => {
  const cartStr = localStorage.getItem("shopping-cart");
  if (cartStr) return JSON.parse(cartStr);

  return null;
};

export const clearShoppingCart = () => {
  localStorage.removeItem("shopping-cart");
};


