import axios from "axios";
import { authHeader } from "./authHeader";

const API_URL = "https://good-food-back.herokuapp.com/goodfood/test/";

export const getPublicContent = () => {
  return axios.get(API_URL + "all");
};

export const getUserBoard = () => {
  return axios.get(API_URL + "user", { headers: authHeader() });
};


