export type ProductType = {
  product_id: string;
  name: string;
  uri: string;
  price?: number;
}
