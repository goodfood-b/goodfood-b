export default interface IUser {
  id?: any | null,
  firstname: string,
  lastname: string,
  birthdate: string,
  email: string,
  password: string,
  roles?: Array<string>
}
