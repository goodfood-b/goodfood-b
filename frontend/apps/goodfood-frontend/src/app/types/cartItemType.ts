export type CartItemType = {
  id: string;
  category: string;
  description: string;
  image: string;
  price: number;
  title: string;
  quantity: number;
}
