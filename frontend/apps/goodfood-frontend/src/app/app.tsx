import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Accueil from './pages/FrontOffice/Accueil/Accueil'
import Restaurants from './pages/FrontOffice/Restaurant/restaurants'
import Categories from './pages/FrontOffice/Categories/Categories'
import Commandes from './pages/BackOffice/Commandes/Commandes'
import CommandesDetails from './pages/BackOffice/CommandesDetail/CommandesDetail'
import Product from './pages/FrontOffice/Product/Product';
import {ShoppingCartProvider} from "./CartUtils/Context/shoppingCartContext";
import Connexion from "./pages/Connexion/connexion";

const App = () => {
  return (
    <ShoppingCartProvider>
      <Routes>
        <Route path="/" element={<Accueil />} />
        <Route path="/accueil" element={<Accueil />} />
        <Route path="/restaurants" element={<Restaurants />} />
        <Route path="/restaurant/produits" element={<Product />} />
        <Route path="/connexion" element={<Connexion />} />
        <Route path="/commandes" element={<Commandes />}/>
        <Route path="/verification/paiement" />
        <Route path="/verification/connexion" />
        <Route path="/categories" />
        <Route path="/commandes/details" element={<CommandesDetails />} />
        <Route path="/categories/:typeCategories" element={<Categories />} />
        <Route path="/aide" />
        <Route path="/contact" />
        <Route path="/apropos" />
        <Route path="/conditionsdutilisation" />
      </Routes>
    </ShoppingCartProvider>
  );
};
export default App;
