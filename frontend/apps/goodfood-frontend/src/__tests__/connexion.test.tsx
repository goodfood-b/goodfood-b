import React from 'react';
import { render, screen, waitFor, within } from '@testing-library/react';
import Connexion from '../app/pages/Connexion/connexion';
import user from '@testing-library/user-event';

const validateEmail = (email = "") => email.includes("@");

describe ('Login form test statements', () => {

  const onSubmit = jest.fn();

  /*beforeEach(() => {
    onSubmit.mockClear();
  });*/

  test('validate function should pass on correct input', () => {
      const mail = "mail@test.com";
      expect(validateEmail(mail)).toBe(true);
  });

  test('validate function should fail on incorrect input', () => {
    const text = "text";
    expect(validateEmail(text)).not.toBe(true);
  });

  test('onSubmit should be called when all fields pass validation', async () => {
    render(<><Connexion /></>);
    user.type(getEmail(), 'mail@test.com');
    user.type(getPassword(), '12345678');
    clickSubmitButton();

    await waitFor(() => {
          expect(onSubmit).toHaveBeenCalledWith({
            email: 'mail@test.com',
            password: '12345678'
          });
    });
  });
});

const getEmail = () => {
  render(
    <>
      <label htmlFor="email">Email</label>
      <input type="text" id="email" />
    </>,
  );
  return screen.getByLabelText('Email');
};

const getPassword = () => {
  render(
      <>
        <label htmlFor="password">Mot de passe</label>
        <input type="password" id="password" />
      </>,
  );
  return screen.getByLabelText('Mot de passe');
};

const clickSubmitButton = () => {
  render(
        <>
           <button type="submit" className="btn-submit" /*disabled={loading}*/>

              <span>Connexion</span>
           </button>
        </>,
  );
  user.click(screen.getByRole('button', {name: /Connexion/i, hidden: true})) ;
};

