module.exports = {
  displayName: 'goodfood-frontend',
  testEnvironment: 'jsdom',
  //preset: '../../jest.preset.js',
  // The root of your source code, typically /src
  // `<rootDir>` is a token Jest substitutes
  roots: ["<rootDir>/src"],
  transform: {
    '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)': '@nrwl/react/plugins/jest',
    '^.+\\.[tj]sx?$': 'ts-jest',
  },
  // Runs special logic, such as cleaning up components  when using React Testing Library and adds special
  // extended assertions to Jest
  setupFilesAfterEnv: ['<rootDir>/src/setup-jest.ts'],
  // Test spec file resolution pattern matches parent folder `__tests__` and filename
  // should contain `test` or `spec`.
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  // Module file extensions for importing
  moduleFileExtensions: ['ts', 'tsx', 'js', "json", "node"],
  coverageDirectory: '../../coverage/apps/goodfood-frontend'
};

