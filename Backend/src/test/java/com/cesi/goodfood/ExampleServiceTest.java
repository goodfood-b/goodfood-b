package com.cesi.goodfood;

import com.cesi.goodfood.dao.ExampleRepository;
import com.cesi.goodfood.model.ExampleProduct;
import com.cesi.goodfood.service.ExampleServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class ExampleServiceTest {
    /* comment for commit */
    @Mock
    private ExampleRepository productRepository;

    @Autowired
    @InjectMocks
    private ExampleServiceImpl productService;
    private ExampleProduct menu;
    private ExampleProduct product;
    private List<ExampleProduct> products;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
        product = new ExampleProduct(new ObjectId(), "nom Produit", "description produit",
                "uri produit", 5, null);
        products = new ArrayList<ExampleProduct>();
        products.add(product);
        menu = new ExampleProduct(new ObjectId(), "Nom menu", "description menu", "uri menu",
                10, products);

        //productRepository.save(menu);
    }

    @AfterEach
    public void rest(){
       product = null;
       menu = null;
       products = null;
    }

    @Test
    public void findAllMenuShouldReturnListOfMenu(){

        given(productRepository.findMenuWithProduct()).willReturn(List.of(menu));

        List<ExampleProduct> list = productService.getAllMenuWithProducts();

        assertThat(list).isNotNull();
        assertThat(list.get(0)).isInstanceOf(ExampleProduct.class);
        assertThat(list.size()).isGreaterThanOrEqualTo(1);
    }
}
