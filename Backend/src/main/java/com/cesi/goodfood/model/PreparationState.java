package com.cesi.goodfood.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document(collection="etat_preparation")
public class PreparationState {

    @MongoId
    @JsonSerialize(using= ToStringSerializer.class)
    private ObjectId _id;

    private EPreparationState name;

    public PreparationState() {
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public EPreparationState getName() {
        return name;
    }

    public void setName(EPreparationState name) {
        this.name = name;
    }
}
