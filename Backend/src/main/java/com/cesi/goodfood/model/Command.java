package com.cesi.goodfood.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;
import java.util.List;

@Document(collection = "commande")
public class Command {

    @MongoId
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId _id;

    @Field(value = "creation")
    private String cmdDate;

    @Field(value = "nomClient")
    private String clientLastName;

    @Field(value = "prenomClient")
    private String clientFirstName;

    @Field(value = "clientRef")
    private ObjectId clientRef;

    @Field(value = "emailClient")
    private String clientEmail;

    @Field(value = "totalCommande")
    private float totalCommande;

    @Field(value="lignesCommande")
    private List<CommandLine> commandLines;

    @DBRef
    private PreparationState preparationStateRef;

    public Command(String cmdDate, String clientLastName, String clientFirstName, ObjectId clientRef,
                   String clientEmail, float totalCommande, List<CommandLine> commandLines
    ) {

        this.cmdDate = cmdDate;
        this.clientLastName = clientLastName;
        this.clientFirstName = clientFirstName;
        this.clientRef = clientRef;
        this.clientEmail = clientEmail;
        this.totalCommande = totalCommande;
        this.commandLines = commandLines;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getCmdDate() {
        return cmdDate;
    }

    public void setCmdDate(String cmdDate) {
        this.cmdDate = cmdDate;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public ObjectId getClientRef() {
        return clientRef;
    }

    public void setClientRef(ObjectId clientRef) {
        this.clientRef = clientRef;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public float getTotalCommande() {
        return totalCommande;
    }

    public void setTotalCommande(float totalCommande) {
        this.totalCommande = totalCommande;
    }

    public PreparationState getPreparationStateRef() {
        return preparationStateRef;
    }

    public void setPreparationStateRef(PreparationState preparationStateRef) {
        this.preparationStateRef = preparationStateRef;
    }

    public List<CommandLine> getCommandLines() {
        return commandLines;
    }

    public void setCommandLines(List<CommandLine> commandLines) {
        this.commandLines = commandLines;
    }
}


