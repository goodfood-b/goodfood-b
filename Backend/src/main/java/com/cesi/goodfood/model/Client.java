package com.cesi.goodfood.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.HashSet;
import java.util.Set;

@Document(collection = "client")
public class Client {

    @MongoId
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId _id;

    @Field(value="nom")
    private String lastName;

    @Field(value="prenom")
    private String firstName;

    @Field(value="email")
    private String email;

    @Field(value="password")
    private String password;

    @Field(value="dateInscription")
    @DateTimeFormat(pattern="dd-MM-yyyy")
    private String signUpDate;

    @Field(value="dateNaissance")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private String birthDate;

    @Field(value="isActive")
    private boolean isActive;

    @DBRef
    private Set<Role> roles = new HashSet<>();


    public Client(ObjectId _id, String lastName, String firstName, String email,
                  String password, String signUpDate, String birthDate, boolean isActive) {

        this._id = _id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.signUpDate = signUpDate;
        this.birthDate = birthDate;
        this.isActive = isActive;
    }

    public Client(String lastName, String firstName, String email, String birthDate, String password) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.birthDate = birthDate;
        this.password = password;

    }

    //@PersistenceConstructor
    public Client(ObjectId _id, String email, String password, Set<Role> roles) {
        this._id = _id;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    @PersistenceConstructor
    public Client(ObjectId _id, String lastName, String firstName, String email, String password, String birthDate) {
        this._id = _id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
    }



    public ObjectId getId() {
        return _id;
    }

    public void setId(ObjectId _id) {
        this._id = _id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public String getSignUpDate() {
        return signUpDate;
    }

    public void setSignUpDate(String signUpDate) {
        this.signUpDate = signUpDate;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + _id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", SignUpDate=" + signUpDate +
                ", birthDate=" + birthDate +
                ", isActive=" + isActive +
                '}';
    }
}