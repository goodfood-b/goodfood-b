package com.cesi.goodfood.model;

public enum ERole {

    ROLE_CLIENT,
    ROLE_EMPLOYEE,
    ROLE_ADMIN
}
