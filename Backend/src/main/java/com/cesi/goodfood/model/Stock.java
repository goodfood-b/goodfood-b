package com.cesi.goodfood.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.*;

import javax.sound.sampled.Port;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@ToString
@Document
public class Stock {
    @Id
    @Field(value = "_id")
    private String id;
    @Field()
    private Integer quantite;
    @Field()
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId restaurant_id;
    @DBRef
    private List<Product> product_id;

    public Stock(String id, Integer quantite, ObjectId restaurant_id, List<Product> product_id) {
        this.id = id ;
        this.quantite = quantite;
        this.restaurant_id = restaurant_id;
        this.product_id = product_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public ObjectId getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(ObjectId restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public List<Product> getProduct_id() {
        return product_id;
    }

    public void setProduct_id(List<Product> product_id) {
        this.product_id = product_id;
    }
}

