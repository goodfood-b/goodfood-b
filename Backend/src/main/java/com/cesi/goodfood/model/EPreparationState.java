package com.cesi.goodfood.model;


public enum EPreparationState {

    NOUVEAU,
    EN_COURS,
    TERMINE,
    ANNULE
}

