package com.cesi.goodfood.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document(collection = "restaurant")
public class Restaurant {
    @MongoId
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    @Field
    private String nom;
    @Field
    private String description;
    @Field
    private String adresse;
    @Field
    private String localisation_gps;
    @Field
    private String url_logo;
    @Field
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId ville_id;

    public Restaurant(ObjectId id, String nom, String description, String adresse, String localisation_gps, String url_logo, ObjectId ville_id) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.adresse = adresse;
        this.localisation_gps = localisation_gps;
        this.url_logo = url_logo;
        this.ville_id = ville_id;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + nom + '\'' +
                ", description='" + description + '\'' +
                ", adresse='" + adresse + '\'' +
                ", localisation_gps='" + localisation_gps + '\'' +
                ", ville_id=" + ville_id +
                '}';
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getLocalisation_gps() {
        return localisation_gps;
    }

    public void setLocalisation_gps(String localisation_gps) {
        this.localisation_gps = localisation_gps;
    }

    public ObjectId getVille_id() {
        return ville_id;
    }

    public String getUrl_logo() {
        return url_logo;
    }

    public void setUrl_logo(String url_logo) {
        this.url_logo = url_logo;
    }

    public void setVille_id(ObjectId ville_id) {
        this.ville_id = ville_id;
    }
}
