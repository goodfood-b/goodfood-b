package com.cesi.goodfood.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Document(collection = "product")
public class ExampleProduct {

    @Id
    @JsonSerialize(using= ToStringSerializer.class)
    private ObjectId _id;

    @Field(value="nom")
    private String name;

    @Field(value="description")
    private String description;

    @Field(value="uri")
    private String uri;

    @Field(value="prix")
    private float price;

    //@Field(value = "produits")
    @DBRef
    private List<ExampleProduct> products;


    public ExampleProduct(ObjectId _id, String name, String description, String uri, float price, List<ExampleProduct> products) {
        this._id = _id;
        this.name = name;
        this.description = description;
        this.uri = uri;
        this.price = price;
        this.products = products;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<ExampleProduct> getProducts() {
        return products;
    }

    public void setProducts(List<ExampleProduct> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + _id +
                ", nom='" + name + '\'' +
                ", description='" + description + '\'' +
                ", prix=" + price +
                ", produits=" + products +
                '}';
    }
}

