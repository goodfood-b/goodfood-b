package com.cesi.goodfood.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Date;

@Document
public class Bill {

    @MongoId
    private Long Id;

    @Field(value="creation_date")
    private Date creationDate;

    @Field(value="payed")
    private boolean isPayed;

    public Bill(Long id, Date creationDate, boolean isPayed) {
        Id = id;
        this.creationDate = creationDate;
        this.isPayed = isPayed;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isPayed() {
        return isPayed;
    }

    public void setPayed(boolean payed) {
        isPayed = payed;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "Id=" + Id +
                ", creationDate=" + creationDate +
                ", isPayed=" + isPayed +
                '}';
    }
}
