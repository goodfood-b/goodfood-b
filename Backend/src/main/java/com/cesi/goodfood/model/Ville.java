package com.cesi.goodfood.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;
@ToString
@Document(collection = "ville")
public class Ville {
    @Id
    private String id;
    @Field
    private String nom;
    @Field
    private String localisation_gps;
    @Field
    private Restaurant restaurants;
    @Field
    private ObjectId pays_id;

    public Ville(String id, String nom, String localisation_gps, Restaurant restaurants, ObjectId pays_id) {
        this.id = id;
        this.nom = nom;
        this.localisation_gps = localisation_gps;
        this.restaurants = restaurants;
        this.pays_id = pays_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLocalisation_gps() {
        return localisation_gps;
    }

    public void setLocalisation_gps(String localisation_gps) {
        this.localisation_gps = localisation_gps;
    }

    public Restaurant getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(Restaurant restaurants) {
        this.restaurants = restaurants;
    }

    public ObjectId getPays_id() {
        return pays_id;
    }

    public void setPays_id(ObjectId pays_id) {
        this.pays_id = pays_id;
    }
}

