package com.cesi.goodfood.model;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

public class CommandLine {

    @Field(value="produit")
    private String name;

    @Field(value="id")
    private ObjectId id;

    @Field(value="prix")
    private float price;

    @Field(value="quantite")
    private int quantity;

    @Field(value="total")
    private float totalLineCommand;

    public CommandLine(String name, ObjectId id, float price, int quantity, float totalLineCommand) {
        this.name = name;
        this.id = id;
        this.price = price;
        this.quantity = quantity;
        this.totalLineCommand = totalLineCommand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getTotalLineCommand() {
        return totalLineCommand;
    }

    public void setTotalLineCommand(float totalLineCommand) {
        this.totalLineCommand = totalLineCommand;
    }
}
