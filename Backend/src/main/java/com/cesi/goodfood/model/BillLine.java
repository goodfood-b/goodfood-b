package com.cesi.goodfood.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document
public class BillLine {

    @MongoId
    private Long id;

    @Field(value="product_price")
    private float productPrice;

    @Field(value="quantity")
    private int quantity;

    public BillLine(Long id, float productPrice, int quantity) {
        this.id = id;
        this.productPrice = productPrice;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "BillLine{" +
                "id=" + id +
                ", productPrice=" + productPrice +
                ", quantity=" + quantity +
                '}';
    }
}
