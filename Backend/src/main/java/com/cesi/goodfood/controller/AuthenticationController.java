package com.cesi.goodfood.controller;

import com.cesi.goodfood.payload.JwtResponse;
import com.cesi.goodfood.payload.LoginRequest;
import com.cesi.goodfood.payload.MessageResponse;
import com.cesi.goodfood.payload.SignupRequest;

import com.cesi.goodfood.service.AuthenticationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/goodfood/auth")
@RestController

public class AuthenticationController {

    private final AuthenticationService authService;

    @Autowired
    public AuthenticationController(AuthenticationService authService) {
        this.authService = authService;
    }

    @PostMapping("/signin")
    public @ResponseBody ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws JsonProcessingException {

        JwtResponse signInResponse = authService.signinUser(loginRequest);

        return new ResponseEntity<>(signInResponse, HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        MessageResponse registerResponse = authService.registerUser(signUpRequest);

        if (registerResponse.getMessage().startsWith("Error")) {
            return ResponseEntity
                    .badRequest()
                    .body(registerResponse.getMessage());
        }

        return ResponseEntity.ok(registerResponse.getMessage());
    }
}