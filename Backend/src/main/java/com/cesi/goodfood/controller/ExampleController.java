package com.cesi.goodfood.controller;

import com.cesi.goodfood.model.ExampleProduct;
import com.cesi.goodfood.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExampleController {

    private final ExampleService service;

    //service is injected by constructor and the constructor method is annotated with '@autowired'
    //to tell Spring Boot to inject that component (bean)
    @Autowired
    public ExampleController(ExampleService service) {
        this.service = service;
    }



    // this method is called when user type http://localhost:8080/products
    @GetMapping("/products")
    public List<ExampleProduct> getProducts(){

        //return service.getAllProducts();
        return service.getAllMenuWithProducts();
    }

    @GetMapping("/menu")
    public ExampleProduct getProduct(){

        return service.getMenu();
    }
}