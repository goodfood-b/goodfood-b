package com.cesi.goodfood.controller;

import com.cesi.goodfood.model.Ville;
import com.cesi.goodfood.service.VilleServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/goodfood/public")
public class VilleController {

    private VilleServiceImpl villeServiceImpl;

    public VilleController(VilleServiceImpl villeServiceImpl){
        this.villeServiceImpl = villeServiceImpl;
    }

    @GetMapping("/villes")
    public List<Ville> allVilles() {
        return this.villeServiceImpl.getAllVilles();
    }

    @GetMapping ("/villes/V1")
    public void villes() {
        this.villeServiceImpl.getVilles();
    }




}
