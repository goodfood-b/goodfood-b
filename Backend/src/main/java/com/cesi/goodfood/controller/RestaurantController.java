package com.cesi.goodfood.controller;

import com.cesi.goodfood.model.Restaurant;
import com.cesi.goodfood.model.Stock;
import com.cesi.goodfood.service.RestaurantServiceImpl;
import com.cesi.goodfood.service.StockServiceImpl;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/goodfood/public")
public class RestaurantController {
    private RestaurantServiceImpl restaurantServiceImpl;
    private StockServiceImpl stockServiceImpl;

    @Autowired
    public RestaurantController(RestaurantServiceImpl restaurantServiceImpl, StockServiceImpl stockServiceImpl) {
        this.restaurantServiceImpl = restaurantServiceImpl;
        this.stockServiceImpl = stockServiceImpl;
    }

    // return all restaurants
    @GetMapping("/restaurants")
    public List<Restaurant> restaurants() {
        List<Restaurant> restaurants = this.restaurantServiceImpl.getAllRestaurants();
        System.out.println(restaurants);
        return restaurants;
    }

    // return one restaurant's information
    @GetMapping("/restaurant/{id}")
    public Optional<Restaurant> getRestaurant(@PathVariable("id") String id) {
        return this.restaurantServiceImpl.getRestaurantById(id);
    }

    // return the a stock for one restaurant
    @GetMapping("/restaurant/{id}/products")
    public List<Stock> getStocks(@PathVariable("id") String id) {
        ObjectId objectIdRestaurant = new ObjectId(id);
        List<Stock> lesStocks = this.stockServiceImpl.findAllStockOneRestaurant(objectIdRestaurant);
        return lesStocks;
    }

    @GetMapping("/restaurant/{id}/products/{categorie}")
    public List<Stock> getStocksCategorie(@PathVariable("id") String id, @PathVariable("categorie") String categorie) {
        System.out.println(categorie);
        ObjectId objectIdRestaurant = new ObjectId(id);
        List<Stock> lesStocks = this.stockServiceImpl.findProductWithCategories(objectIdRestaurant, categorie);
        return lesStocks;
    }
}