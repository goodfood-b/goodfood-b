package com.cesi.goodfood.controller;

import com.cesi.goodfood.model.Product;
import com.cesi.goodfood.service.ProductServiceImpl;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/goodfood/public")
public class ProductController {
    private ProductServiceImpl productServiceImpl;

    public ProductController(ProductServiceImpl productServiceImpl){
        this.productServiceImpl = productServiceImpl;
    }

    @GetMapping("/products")
    public List<Product> getAllProducts() {
        return this.productServiceImpl.getAllProducts();
    }
}
