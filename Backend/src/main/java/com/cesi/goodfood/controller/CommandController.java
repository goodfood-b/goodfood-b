package com.cesi.goodfood.controller;

import com.cesi.goodfood.model.Command;
import com.cesi.goodfood.payload.CartRequest;
import com.cesi.goodfood.payload.MessageResponse;
import com.cesi.goodfood.service.CommandService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/goodfood/public")
@RestController
@Tag(name="Order")
public class CommandController {

    private final CommandService commandService;


    @Autowired
    public CommandController(CommandService commandService) {

        this.commandService = commandService;
    }

    @PostMapping("/savecommand")
    @Operation(summary="Save order", responses = {
            @ApiResponse(description= "Create a new order in database", responseCode = "200",
                    content= @Content(mediaType="application/json", schema = @Schema(implementation = Command.class))),
            @ApiResponse(responseCode = "400", description = "Bad request", content= @Content),
            @ApiResponse(responseCode = "406", description = "Contenu non approprié", content = @Content)
    })
    public ResponseEntity<?> saveCommand(@Parameter(name="Cart Request", description="order to be saved. Cannot be null", required=true)
                                             @Valid @RequestBody CartRequest cartRequest){

        MessageResponse messageResponse = commandService.saveCommand(cartRequest);

        return ResponseEntity.ok(messageResponse);
    }

    private CommandService service;
    //private CommandServiceImpl service2;



    @Operation(summary="Get all orders", responses = {
            @ApiResponse(description= "Create a new order in database", responseCode = "200",
                    content= @Content(mediaType="application/json", schema = @Schema(implementation = Command.class))),
            @ApiResponse(responseCode = "400", description = "Bad request", content= @Content),
            @ApiResponse(responseCode = "406", description = "Contenu non approprié", content = @Content)
    })
    @GetMapping("/commands")
    public List<Command> getCommands(){

        return commandService.getAllCommands();
    }

    @GetMapping("/commandsWithClient")
    public List<Command> getCommandsWithClient(){
        return this.commandService.getAllCommandsWithClient();
    }

    @GetMapping("/commandsWithEtatPreparation")
    public List<Command> getCommandsWithEtatPreparation(){
        return commandService.getAllCommandsWithEtatPreparation();
    }

    @GetMapping("/commandsWithProducts")
    public List<Command> getCommandsWithProducts(){
        return commandService.getAllCommandsWithProducts();
    }

    @GetMapping("commands/{etatPreparation}")
    public Optional<Command> getProduct(@PathVariable("etatPreparation") String etatPreparation) {
        //return this.service2.getAllCommandsWithEtatPreparation1(etatPreparation);

    return null ;}



}

