package com.cesi.goodfood.controller;

import com.cesi.goodfood.model.Stock;
import com.cesi.goodfood.service.StockServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/goodfood/public")
public class StockController {
    private StockServiceImpl stockServiceImpl;

    @Autowired
    public StockController(StockServiceImpl stockServiceImpl) { this.stockServiceImpl = stockServiceImpl; }

    // return all stocks
    @GetMapping("/stocks")
    public List<Stock> getStocks() {
        return this.stockServiceImpl.findStocks();
    }

    // add new stock
    @PostMapping("/stock")
    public Stock setStock(@RequestBody Stock stock) {
        return this.stockServiceImpl.setStock(stock);
    }

    // return one stock
    @GetMapping("stock/{id}")
    public Optional<Stock> getStock(@PathVariable("id") String id) {
        return this.stockServiceImpl.findOneStock(id);
    }
}
