package com.cesi.goodfood.dao;

import com.cesi.goodfood.model.EPreparationState;
import com.cesi.goodfood.model.PreparationState;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface PreparationStateRepository extends MongoRepository<PreparationState, String> {

    PreparationState findByName(EPreparationState name);
}
