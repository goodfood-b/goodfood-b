package com.cesi.goodfood.dao;

import com.cesi.goodfood.model.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<Client, String> {

    Optional<Client> findByEmail(String userName);

    Boolean existsByEmail(String email);
}
