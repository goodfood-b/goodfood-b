package com.cesi.goodfood.dao;

import com.cesi.goodfood.model.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AuthenticationRepository extends MongoRepository<Client, Long> {
}
