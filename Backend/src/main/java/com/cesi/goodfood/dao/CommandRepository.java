package com.cesi.goodfood.dao;

import com.cesi.goodfood.model.Command;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.UnwindOperation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.repository.Query;


import java.util.List;
import java.util.Optional;


@Repository
public interface CommandRepository extends MongoRepository<Command, String> {

    List<Command> findAll() ;

    @Aggregation(pipeline = {"{$lookup: { from:\"client\", localField:\"client_id\", foreignField:\"_id\", as:\"client\" }}"})
    List<Command> findAllWithClient();

    @Aggregation(pipeline = {" {$lookup: { from:'client', localField:\"client_id\", foreignField:\"_id\", as:'client' }}, { $unwind: '$client'},{$lookup: { from:\"etat_preparation\", localField:\"etat_preparation_id\", foreignField:\"_id\", as:'etat_preparation' }, { $unwind: '$etat_preparation'}"})
    List<Command> findAllWithEtatPreparation();

    @Aggregation(pipeline = {"{$lookup: { from:\"client\", localField:\"client_id\", foreignField:\"_id\", as:\"client\" }}, {$lookup: { from:\"etat_preparation\", localField:\"etat_preparation_id\", foreignField:\"_id\", as:\"etat_preparation\" }} "})
    List<Command> findAllWithClientAndEtatPreparation();

    @Aggregation(pipeline = {"{$lookup: { from:\"client\", localField:\"client_id\", foreignField:\"_id\", as:\"client\" }}, {$lookup: { from:\"etat_preparation\", localField:\"etat_preparation_id\", foreignField:\"_id\", as:\"etat_preparation\" }} "})
    List<Command> findAllWithProduct();

    @Aggregation(pipeline = {"{$lookup: { from:\"client\", localField:\"client_id\", foreignField:\"_id\", as:\"client\" }}, {$lookup: { from:\"etat_preparation\", localField:\"etat_preparation_id\", foreignField:\"_id\", as:\"etat_preparation\" }}, {$lookup: { from:\"panier\", localField:\"panier_id\", foreignField:\"_id\", as:\"panier\" }}, {$lookup: { from:\"product\", localField:\"products\", foreignField:\"_id\", as:\"items\" }},{$project: { client_id\": 0, \"etat_preparation_id\": 0, \"panier_id\": 0}}"})
    List<Command> findAllWithProducts();

    //Optional<Command> findById(String id);
    //@Query("{'etatPreparation' : ?1 }")
    //@Aggregation(" {$lookup: { from:'client', localField:\"client_id\", foreignField:\"_id\", as:'client' }}, { $unwind: '$client'},{$lookup: { from:\"etat_preparation\", localField:\"etat_preparation_id\", foreignField:\"_id\", as:'etat_preparation' }, { $unwind: '$etat_preparation'}")
    //List<Command> findAllWithEtatPreparation1(String etatPreparation);




    @Aggregation(pipeline = {" {$lookup: { from:'client', localField:\"client_id\", foreignField:\"_id\", as:'client' }}, { $unwind: '$client'},{$lookup: { from:\"etat_preparation\", localField:\"etat_preparation_id\", foreignField:\"_id\", as:'etat_preparation' }, { $unwind: '$etat_preparation'}"})
    Optional<Command> findAllWithEtatPreparation1(String etatPreparation);







}
