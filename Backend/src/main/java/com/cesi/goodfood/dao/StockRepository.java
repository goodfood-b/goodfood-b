package com.cesi.goodfood.dao;

import com.cesi.goodfood.model.Stock;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StockRepository extends MongoRepository<Stock, String> {
    @Aggregation("{$lookup: {from: \"product\", localField: \"product_id\", foreignField: \"_id\", as: \"product_id\"}}, {$lookup: { from: \"restaurant\", localField: \"restaurant_id\", foreignField: \"_id\", as: \"restaurant_id\" }}")
    List<Stock> findStocksWithProducts();

    // @Query("{'restaurant_id' : ?0 }")
    @Aggregation({"{$match: {'restaurant_id':  ?0 }}", "{$lookup:  {from: \"product\", localField: \"product_id\", foreignField: \"_id\", as: \"product_id\"}}"})
    List<Stock> findAllByRestaurantID(ObjectId id);

    // renvoi le stocks de produits en fonciton de la categorie pour un restaurant
    Optional<Stock> findById(String id);
    @Query("{'restaurant_id' : ?0, 'product_id.categorie': ?1}")
    @Aggregation("{$lookup:  {from: \"product\", localField: \"product_id\", foreignField: \"_id\", as: \"product_id\"}}")
    List<Stock> findAllStocksForRestaurantByCategorie(ObjectId id, String categorie);
}
