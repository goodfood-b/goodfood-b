package com.cesi.goodfood.dao;

import com.cesi.goodfood.model.Ville;
import com.cesi.goodfood.service.VilleServiceImpl;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface VilleRepository extends MongoRepository<Ville, String> {

    public List<Ville> findAll();

    @Aggregation("[{'$unwind': {'path': '$restaurants'}}, { '$lookup': { 'from': 'restaurant', 'localField': 'restaurants', 'foreignField': '_id', 'as': 'restaurants'}}]")
    public List<Ville> findCustomVilles();
}
