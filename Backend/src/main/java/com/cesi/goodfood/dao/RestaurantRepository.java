package com.cesi.goodfood.dao;

import com.cesi.goodfood.model.Restaurant;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RestaurantRepository extends MongoRepository<Restaurant, String> {
    List<Restaurant> findAll();

    @Query("{ 'nom' : ?0 }")
    Restaurant findByName(String name);

    Optional<Restaurant> findById(String id);
}
