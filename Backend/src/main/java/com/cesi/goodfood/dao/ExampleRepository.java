package com.cesi.goodfood.dao;

import com.cesi.goodfood.model.ExampleProduct;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface ExampleRepository extends MongoRepository<ExampleProduct, String>{

    List<ExampleProduct> findAll() ;
    @Aggregation(pipeline="{$match: {_id: ObjectId(\"6299128cfa970557fa7b3596\")}}, {$lookup: { from:\"product\", localField:\"produits\", foreignField:\"_id\", as:\"items\" }}, { $project: { _id: 1, nom: 1, description: 1, prix: 1, uri: 1, items: 1 }}")
    ExampleProduct findAllProductsFromMenu ();

    @Aggregation("{$lookup: { from:\"product\", localField:\"produits\", foreignField:\"_id\", as:\"produits\" }}, { $project: { _id: 1, nom: 1, description: 1, prix: 1, uri: 1, produits: 1 }}")
    List<ExampleProduct> findMenuWithProduct();


}

