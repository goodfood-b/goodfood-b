package com.cesi.goodfood;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@CrossOrigin(origins = "hhtp://localhost:8080/swagger-ui/index.html")
@SpringBootApplication(scanBasePackages = "com.cesi.goodfood")
public class GoodfoodApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoodfoodApplication.class, args);
    }

    @Bean
    public OpenAPI openApiConfig(){
        return new OpenAPI().info(apiInfo());
    }

    private Info apiInfo(){
        Info info = new Info();
        info.title("GoodFood API Documentation")
                .description("description of the API endpoints")
                .version("Version 0.0.9");

        return info;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/swagger-ui/index.html").allowedOrigins("http://localhost:8080");
            }
        };
    }

}
