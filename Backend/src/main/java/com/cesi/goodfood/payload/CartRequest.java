package com.cesi.goodfood.payload;

import com.cesi.goodfood.model.CommandLine;
import org.bson.types.ObjectId;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

public class CartRequest {

    @NotBlank
    private String clientLastName;

    @NotBlank
    private String clientFirstName;

    @NotNull
    private ObjectId clientRef;

    @NotBlank
    @Email
    private String clientEmail;

    @NotNull
    private List<CartLine> commandLines;

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public ObjectId getClientRef() {
        return clientRef;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public void setClientRef(ObjectId clientRef) {
        this.clientRef = clientRef;
    }

    public List<CartLine> getCommandLines() {
        return commandLines;
    }

    public void setCommandLines(List<CartLine> commandLines) {
        this.commandLines = commandLines;
    }
}
