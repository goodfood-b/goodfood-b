package com.cesi.goodfood.service;

import com.cesi.goodfood.dao.ExampleRepository;
import com.cesi.goodfood.model.ExampleProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/* This class simply calls the repository method.*/
@Service
public class ExampleServiceImpl implements ExampleService {


    private ExampleRepository productRepository;

    //repository is injected by constructor and the constructor method is annotated with '@autowired'
    //to tell Spring Boot to inject that component (bean)
    @Autowired
    public ExampleServiceImpl(ExampleRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ExampleProduct> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public ExampleProduct getMenu(){
        return productRepository.findAllProductsFromMenu();
    }

    @Override
    public List<ExampleProduct> getAllMenuWithProducts() {
        return productRepository.findMenuWithProduct();
    }
}


