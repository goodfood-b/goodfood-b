package com.cesi.goodfood.service;

import com.cesi.goodfood.dao.RestaurantRepository;
import com.cesi.goodfood.model.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    private RestaurantRepository restaurantRepository;

    @Autowired
    public RestaurantServiceImpl(RestaurantRepository prestaurantRepository) {
        this.restaurantRepository = prestaurantRepository;
    }

    @Override
    public List<Restaurant> getAllRestaurants() {
        return this.restaurantRepository.findAll();
    }
    @Override
    public Restaurant getRestaurant(String name) {
        return this.restaurantRepository.findByName(name);
    }

    @Override
    public Optional<Restaurant> getRestaurantById(String id) {
        return this.restaurantRepository.findById(id);
    }
}
