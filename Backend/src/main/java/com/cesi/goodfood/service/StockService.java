package com.cesi.goodfood.service;

import com.cesi.goodfood.model.Stock;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface StockService {
    List<Stock> findStocks();
    List<Stock> findAllStockOneRestaurant(ObjectId id);
    Stock setStock(Stock stock);
    Optional<Stock> findOneStock(String id);
}
