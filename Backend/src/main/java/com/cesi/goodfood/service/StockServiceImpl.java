package com.cesi.goodfood.service;

import com.cesi.goodfood.dao.StockRepository;
import com.cesi.goodfood.model.Stock;
import com.mongodb.client.MongoClient;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class StockServiceImpl implements StockService{
    private StockRepository stockRepository;

    @Autowired
    public StockServiceImpl(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Override
    public List<Stock> findStocks() {
        return this.stockRepository.findStocksWithProducts();
    }

    @Override
    public List<Stock> findAllStockOneRestaurant(ObjectId id) {
        return this.stockRepository.findAllByRestaurantID(id);
    }

    public List<Stock> findProductWithCategories(ObjectId id, String categorie) {
        return this.stockRepository.findAllStocksForRestaurantByCategorie(id, categorie);
    }

    @Override
    public Stock setStock(Stock stock) {
        return this.stockRepository.insert(stock);
    }

    @Override
    public Optional<Stock> findOneStock(String id) {
        return this.stockRepository.findById(id);
    }
}
