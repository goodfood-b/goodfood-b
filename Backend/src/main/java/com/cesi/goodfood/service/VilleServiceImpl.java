package com.cesi.goodfood.service;

import com.cesi.goodfood.dao.VilleRepository;
import com.cesi.goodfood.model.Ville;
import jdk.dynalink.linker.support.Lookup;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class VilleServiceImpl implements VilleService{

    private VilleRepository villeRepository;
    private MongoTemplate mongoTemplate;

    public VilleServiceImpl (VilleRepository villeRepository, MongoTemplate mongoTemplate) {
        this.villeRepository = villeRepository;
        this.mongoTemplate = mongoTemplate;
    }
    @Override
    public List<Ville> getAllVilles() {
        return this.villeRepository.findCustomVilles();
    }

    @Override
    public void getVilles() {

    }
}
