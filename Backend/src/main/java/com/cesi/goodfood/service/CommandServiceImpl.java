package com.cesi.goodfood.service;

import com.cesi.goodfood.dao.CommandRepository;
import com.cesi.goodfood.dao.PreparationStateRepository;
import com.cesi.goodfood.payload.CartRequest;
import com.cesi.goodfood.payload.MessageResponse;
import com.cesi.goodfood.model.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cesi.goodfood.model.CommandLine;
import com.cesi.goodfood.model.EPreparationState;
import com.cesi.goodfood.model.PreparationState;
import com.cesi.goodfood.payload.CartLine;
import org.bson.types.ObjectId;
import java.util.Optional;

@Service
public class CommandServiceImpl implements CommandService {

    @Autowired
    private CommandRepository commandRepository;

    @Autowired
    PreparationStateRepository preparationStateRepository;


    @Autowired
    public CommandServiceImpl(CommandRepository commandRepository){
        this.commandRepository = commandRepository;
    }



    @Override
    public List<Command> getAllCommands(){
        return commandRepository.findAll();
    }


    @Override
    public List<Command> getAllCommandsWithClient() {
        return this.commandRepository.findAllWithClient();
    }



    @Override
    public Optional<Command> getAllCommandsWithEtatPreparation1(String etatPreparation) {
        return this.commandRepository.findAllWithEtatPreparation1(etatPreparation);
    }

    @Override
    public List<Command> getAllCommandsWithEtatPreparation() {
        return commandRepository.findAllWithEtatPreparation();
    }

    @Override
    public List<Command> getAllCommandsWithProducts() {
        return commandRepository.findAllWithProducts();
    }

    @Override
    public MessageResponse saveCommand(CartRequest cartRequest) {

        String commandDate = new Date().toString();
        List<CommandLine> lines = new ArrayList<>();
        float commandTotal = 0;

        for(CartLine cl : cartRequest.getCommandLines()){
            String name = cl.getName();
            ObjectId productRef = cl.getId();
            float price = cl.getPrice();
            int quantity = cl.getQuantity();
            float totalLine = price * quantity;
            commandTotal += totalLine;
            CommandLine commandLine = new CommandLine(name, productRef, price, quantity, totalLine);
            lines.add(commandLine);
        }
        Command command = new Command(commandDate, cartRequest.getClientLastName(), cartRequest.getClientFirstName(),
                                        cartRequest.getClientRef(), cartRequest.getClientEmail(),
                                        commandTotal, lines);

        PreparationState newState = preparationStateRepository.findByName(EPreparationState.NOUVEAU);
        command.setPreparationStateRef(newState);
        commandRepository.save(command);

        return new MessageResponse("Votre commande a bien été prise en compte");
    }
}

