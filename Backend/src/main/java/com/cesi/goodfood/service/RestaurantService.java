package com.cesi.goodfood.service;

import com.cesi.goodfood.model.Restaurant;

import java.util.List;
import java.util.Optional;

public interface RestaurantService {
    List<Restaurant> getAllRestaurants();
    Restaurant getRestaurant(String name);
    Optional<Restaurant> getRestaurantById(String id);
}
