package com.cesi.goodfood.service;

import com.cesi.goodfood.model.Product;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> getAllProducts();
}

