package com.cesi.goodfood.service;

import com.cesi.goodfood.model.Command;
import com.cesi.goodfood.payload.CartRequest;
import com.cesi.goodfood.payload.MessageResponse;

import java.util.List;
import java.util.Optional;

public interface CommandService {
    MessageResponse saveCommand(CartRequest cartRequest);

    List<Command> getAllCommands();
    List<Command> getAllCommandsWithClient();
    //List<Command> getAllCommandsWithProduct(String etatPreparation);
    Optional<Command> getAllCommandsWithEtatPreparation1(String etatPreparation);

    List<Command> getAllCommandsWithEtatPreparation();
    List<Command> getAllCommandsWithProducts();
}
