package com.cesi.goodfood.service;

import com.cesi.goodfood.model.Ville;

import java.util.List;

public interface VilleService {
    public List<Ville> getAllVilles();
    public void getVilles();
}
