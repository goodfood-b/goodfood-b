package com.cesi.goodfood.service;

import com.cesi.goodfood.model.ExampleProduct;

import java.util.List;

/* this interface contains all the method that the service implementation must override. it participates to
Inversion Of Control mechanism. With this mechanism, we delegate the responsibility of instantiate object,
here the service implementation, to the abstracted class here the service interface*/

public interface ExampleService {

    List<ExampleProduct> getAllProducts();
    ExampleProduct getMenu();
    List<ExampleProduct> getAllMenuWithProducts();
}

