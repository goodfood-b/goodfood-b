package com.cesi.goodfood.service;

import com.cesi.goodfood.payload.JwtResponse;
import com.cesi.goodfood.payload.LoginRequest;
import com.cesi.goodfood.payload.MessageResponse;
import com.cesi.goodfood.payload.SignupRequest;

public interface AuthenticationService {

    JwtResponse signinUser(LoginRequest loginRequest);
    MessageResponse registerUser(SignupRequest signupRequest);

}
