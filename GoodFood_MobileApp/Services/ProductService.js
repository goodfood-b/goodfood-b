export default class ProductService{
    // Bouchon pour simuler un appel http
    static GetAll(){
        return [
            {
                "Id":1,
                "Nom":"Burger Bacon de la guingette"
            },
            {
                "Id":2,
                "Nom":"Wrap Thon"
            },
            {
                "Id":3,
                "Nom":"Tortilla"
            },
            {
                "Id":4,
                "Nom":"Brocoli"
            }
        ]
    } 
}