import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../Views/HomeScreen';
import ProfilScreen from '../Views/ProfilScreen';
import { connect } from 'react-redux';
import React from 'react';
import AuthScreen from '../Views/AuthScreen';
import { TouchableOppacity, Text } from 'react-native';
const Tab = createBottomTabNavigator();

class Tabs extends React.Component {
  
  constructor(props){
    super(props);
  }

  _logout(){
    const action = {type:"SIGN_OUT"};
    this.props.dispatch(action);
  }
  
  render(){
    if(this.props.userToken == null){
      return(
        <Tab.Navigator> 
          <Tab.Screen name="Connexion" component={AuthScreen} />
        </Tab.Navigator>
      )
    }
    else{
      return(
        <Tab.Navigator> 
          <Tab.Screen name="Accueil" component={HomeScreen} />
          <Tab.Screen name="Profil" component={ProfilScreen} />
        </Tab.Navigator>
      )
    }      
  }
}

const mapStateToProps = (state) => {
  return {
    userToken : state.userToken
  }
}

export default connect(mapStateToProps)(Tabs)