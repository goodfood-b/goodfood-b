import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Tabs from './Navigation/Navigation';
import { Provider } from 'react-redux';
import Store from './Store/configureStore';

export default function App() {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Tabs></Tabs>
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
