import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
export default class ProductItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }
    render(){
        return(
            <View style = {Styles.wrapper} >
                <Text style = {Styles.ProductName}>
                    {this.props.productName}
                </Text>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    wrapper : {
        flex:1,
        backgroundColor:"white",
        width:"90%",
        margin:"5%",
        justifyContent:"center",
        alignItems:"center",
        height:150
    },
    ProductName : {
        fontSize:20,
        fontWeight:"bold"
    }
})