const initialState  = {
    isLoading: true,
    isSignout: false,
    userToken: null,
  };

function toggleUser(state = initialState,action){
    let nextState;
    switch (action.type) {
      case 'RESTORE_TOKEN':
        nextState = {
          ...state,
          userToken: action.token,
          isLoading: false,
        };
        return nextState || state
        case 'SIGN_IN':
          nextState = {
            ...state,
            isSignout: false,
            userToken: action.token,
          }
        return nextState || state
        case 'SIGN_OUT':
          nextState = {
            ...state,
            isSignout: true,
            userToken: null,
          }
          return nextState || state
          default:
              return state
      }

}

export default toggleUser
