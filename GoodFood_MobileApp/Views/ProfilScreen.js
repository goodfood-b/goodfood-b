import { Text, TouchableOpacity, View, StyleSheet } from "react-native";
import React from 'react';
import { connect } from "react-redux";
 class ProfilScreen extends React.Component{
    constructor(props){
        super(props);
        this.state = {}
    }

    _logout(){
        const action = {type:"SIGN_OUT"};
        this.props.dispatch(action);
    }

    render(){
        return(
            <View style={Styles.main}>
                <TouchableOpacity 
                    style={Styles.button}
                    onPress={()=>{this._logout()}}
                >
                    <Text style={Styles.text_button}>Déconnexion</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    main:{
        flex:1
    },
    button:{

    },
    text_button : {
        fontSize : 16
    }
})


const mapStateToProps = (state) => {
    return state
  }

export default connect(mapStateToProps)(ProfilScreen)