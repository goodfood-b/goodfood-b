import React from 'react'
import { ScrollView, View, Text, StyleSheet } from 'react-native'
import ProductItem from '../Components/ProductItem';
import ProductService from '../Services/ProductService';

// Création d'un component "Home"
export default class HomeScreen extends React.Component{
    // Constructeur déclarant les propriétés et l'état du component
    constructor(props){
        super(props);
        this.state = {}
    }
    productsRender(){
        let productsList = []
       ProductService.GetAll().forEach(product => {   
           productsList.push(
               <ProductItem productName = {product.Nom} key={product.Id}></ProductItem>
           )
       });
       return productsList
    }
    // La fonction render renvoie la vue du composant    
    render(){
        return(
            // Ouverture d'une balise View ( = div du html) et affactation d'un style
            <ScrollView style={Styles.main}>
                {this.productsRender()}
            </ScrollView>
        )
    }
}

// Définition des styles du composant
const Styles = StyleSheet.create({
    main : {
        flex:1
    }
})