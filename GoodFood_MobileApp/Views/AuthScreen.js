import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
class AuthScreen extends React.Component{
    constructor(props){
        super(props);
    }

    login(){
        const action = {type:"SIGN_IN", token:"28bx3yu"};
        this.props.dispatch(action)
    }

    render(){
        return(
            <View style={Styles.main}>
                <TouchableOpacity
                 style={Styles.connexion_button}
                 onPress={()=>{this.login()}}
                >
                    <Text style={Styles.text_connexion_button}> 
                        Se connecter avec google
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    main:{
        flex:1,
        height:"100%",
        display:"flex",
        justifyContent:"center",
        alignItems:"center"
    },
    connexion_button:{
        width:"90%",
        backgroundColor:"lightgrey",
        height:40,
        display:"flex",
        justifyContent:"center",
        alignItems:"center"
    },
    text_connexion_button:{
        fontSize:20,
        fontWeight:"bold"
    }
})

const mapStateToProps = (state) => {
    return {
      isSignout: state.isSignout
    }
  }

export default connect(mapStateToProps)(AuthScreen)